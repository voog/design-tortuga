{% assign type = block.type %}
{% assign data = block.data %}
{% assign id = block.id %}

<div class="container articles-container" data-type="{{ type }}" data-id="{{ id }}" data-value="{{ data | json | escape }}" style="background-color:{{ data.color }}">
  {% if editmode %}
    <button class="bg-settings-toggle" data-color="{{ data.colorData | json | escape }}"></button>
    <button disabled class="js-settings-btn" data-type="{{ type }}" data-value="{{ data.count }}">Settings</button>
    <button class="edy-cbtn js-delete-btn"><span><span class="edy-cbtn-content"><span>Delete</span></span></span></button>
  {% endif %}

  {% assign method = 'latest_' | append: data.count | append: '_articles' %}
  {% assign articles_with_limit = site[method] %}
  {% assign name = "content_" | append: id | append: "-" | append: '1' %}

  <div class="formatted-content fullwidth" data-name="{{ name }}">{% content name=name %}</div>
  {% for article in articles_with_limit %}
    {% include 'article_box' article: article, editable: false %}
  {% endfor %}
</div>
