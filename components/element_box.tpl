<section class="element">
  <h2>{% if editable %}{% editable element.title %}{% else %}<a href="{{ element.url }}">{{ element.title }}{% endif %}</a></h2>
  <p>{% if editable %}{% editable element.body %}{% else %}{{ element.body }}{% endif %}</p>
  <p>{% if editable %}{% editable element.image target_width="420" %}{% else %}<img src="{{ element.image }}" />{% endif %}</p>
</section>
