{% assign type = block.type %}
{% assign data = block.data %}
{% assign id = block.id %}

<div class="container" data-type="{{ type }}" data-id="{{ id }}" style="background-color:{{ data.color }}">
  {% if editmode %}
    <button class="bg-settings-toggle" data-color="{{ data.colorData | json | escape }}"></button>
    <button disabled class="js-settings-btn" data-type="{{ type }}" data-value="{{ data.count }}">Settings</button>
    <button class="edy-cbtn js-delete-btn"><span><span class="edy-cbtn-content"><span>Delete</span></span></span></button>
  {% endif %}
  
  {% assign count = data.count %}
  {% for i in (1..count) %}
    {% assign name = "content_" | append: id | append: "-" | append: i %}
    {% assign class = "split-" | append: count %}

    <div class="formatted-content column {{ class }}" data-name="{{ name }}">{% content name=name %}</div>
  {% endfor %}
</div>
