<section class="article">
  <h2 title="{{ article.created_at | date: 'YYYY-MM-DD' }}">{% if editable %}{% editable article.title %}{% else %}<a href="{{ article.url }}">{{ article.title }}{% endif %}</a></h2>
  <p>{% if editable %}{% editable article.excerpt %}{% else %}{{ article.excerpt }}{% endif %}</p>
  {% if show_body %}<p>{% if editable %}{% editable article.body %}{% else %}{{ article.body}}{% endif %}</p>{% endif %}
</section>
