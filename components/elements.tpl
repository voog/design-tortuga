{% assign type = block.type %}
{% assign data = block.data %}
{% assign id = block.id %}

<div class="container elements-container" data-type="{{ type }}" data-id="{{ id }}" data-value="{{ data | json | escape }}" style="background-color:{{ data.color }}">
  {% if editmode %}
    <button class="bg-settings-toggle" data-color="{{ data.colorData | json | escape }}"></button>
    <button disabled class="js-settings-btn" data-type="{{ type }}" data-value="{{ data.count }}">Settings</button>
    <button class="edy-cbtn js-delete-btn"><span><span class="edy-cbtn-content"><span>Delete</span></span></span></button>
  {% endif %}

  {% assign name = "content_" | append: id | append: "-" | append: '1' %}

  <div class="formatted-content fullwidth" data-name="{{ name }}">{% content name=name %}</div>
  {% elementscontext edicy_page_path=data.path %}
    {% for element in elements %}
      {% include 'element_box' element: element, editable: false %}
    {% endfor %}
  {% endelementscontext %}
</div>
