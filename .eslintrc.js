module.exports = {
  globals: {
    test: 1,
    expect: 1
  },
  extends: ['airbnb-base', 'prettier'],
  plugins: ['prettier'],
  parserOptions: {
    sourceType: 'module',
    ecmaFeatures: {
      jsx: false,
    },
  },
  env: {
    es6: true,
    node: false,
  },
  rules: {
    'prettier/prettier': [
      'warn',
      {
        singleQuote: true,
        trailingComma: 'all',
        printWidth: 120,
        bracketSpacing: false,
      },
    ],
    curly: ['error', 'all'],
    'no-confusing-arrow': 'error',
    'no-tabs': 'error',
  },
};
