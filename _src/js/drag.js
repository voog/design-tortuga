import dragula from 'dragula';

export function initDrag() {
  return dragula(Array.from(document.querySelectorAll('.container-list')), {
    moves(el, source, handle, sibling) {
      return el.classList.contains('container');
    },

    revertOnSpill: true,
    removeOnSpill: false,
    ignoreInputTextSelection: true,
  });
}
