import {reloadPage, getElementID} from '../utils';

const options = {
  min: 1,
  max: 5
};

const texts = {
  title: 'Blog articles'
};

function construct(opts) {
  return {
    type: 'articles',
    data: {
      count: opts.count,
      path: 'blog'
    }
  };
}

function initEditor(btn, pageData, structure) {
  const data = btn.getAttribute('data-value');
  const id = getElementID(btn);
  const start = options.min;
  const end = options.max;
  const length = end - start + 1;

  btn.removeAttribute('disabled');
  return new Edicy.SettingsEditor(btn, {
    menuItems: [
      {
        title: texts.title,
        type: 'select',
        key: 'count',
        list: Array.from(Array(length).keys()).map(idx => {
          const n = idx + start;
          return {
            title: String(n),
            value: n
          };
        })
      }
    ],

    values: {
      count: parseInt(data, 10)
    },

    commit(data) {
      const idx = structure.indexOf(id);
      const updatedEl = Object.assign({}, structure.get(idx), {data: {count: data.count}});
      structure.update(idx, updatedEl);
      pageData.set(
        'structure',
        {
          structure: structure.value
        },
        {
          success: reloadPage
        }
      );
    }
  });
}

export default {
  texts,
  options,
  default: construct({count: 3}),
  construct,
  initEditor
};
