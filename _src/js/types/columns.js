import {reloadPage, getElementID} from '../utils';

const options = {
  min: 1,
  max: 3
};

const texts = {
  title: 'Vertical columns'
};

function construct(opts) {
  return {
    type: 'columns',
    data: {
      count: opts.count
    }
  };
}

function initEditor(btn, pageData, structure) {
  const data = btn.getAttribute('data-value');
  const id = getElementID(btn);
  const start = options.min;
  const end = options.max;
  const length = end - start + 1;

  btn.removeAttribute('disabled');
  return new Edicy.SettingsEditor(btn, {
    menuItems: [
      {
        title: texts.title,
        type: 'select',
        key: 'count',
        list: Array.from(Array(length).keys()).map(idx => {
          const n = idx + start;
          return {
            title: String(n),
            value: n
          };
        })
      }
    ],

    values: {
      count: parseInt(data, 10)
    },

    commit(data) {
      const idx = structure.indexOf(id);
      const el = Object.assign({}, structure.get(idx));
      el.data = el.data || {};
      el.data.count = data.count;
      structure.update(idx, el);
      console.log(structure.value);
      pageData.set(
        'structure',
        {
          structure: structure.value
        },
        {
          success: reloadPage
        }
      );
    }
  });
}

export default {
  texts,
  options,
  default: construct({count: 1}),
  construct,
  initEditor
};
