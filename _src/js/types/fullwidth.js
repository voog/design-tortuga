function construct() {
  return {
    type: 'fullwidth',
  };
}

export default {
  texts: {
    title: 'Full width content',
  },
  options: {},
  default: construct(),
  construct,
};
