import Columns from './types/columns';
import Articles from './types/articles';
import Elements from './types/elements';
import FullWidth from './types/fullwidth';

export class Structure {
  constructor(data) {
    if (!data) {
      data = [];
    }
    this.data = [...data];
  }

  get value() {
    return this.data;
  }

  get(idx) {
    return this.data[idx];
  }

  update(idx, value) {
    if (idx < 0 || idx >= this.data.length) {
      return this;
    }
    this.data[idx] = Object.assign({}, this.data[idx], value);
    return this;
  }

  replace(idx, value) {
    this.data[idx] = value;
    return this;
  }

  getNextID() {
    if (this.data.length === 0) {
      return 1;
    }
    const ids = this.data.map((e, idx) => (typeof e.id !== 'undefined' ? e.id : idx));
    return Math.max(...ids) + 1;
  }

  push(value) {
    const idMissing = typeof value.id === 'undefined';
    this.data.push(Object.assign(value, idMissing ? {id: this.getNextID()} : {}));
    return this;
  }

  unshift(value) {
    const idMissing = typeof value.id === 'undefined';
    this.data.unshift(Object.assign(value, idMissing ? {id: this.getNextID()} : {}));
    return this;
  }

  delete(idx) {
    if (idx === -1) {
      return this;
    } else if (idx <= 1 && this.value.length === 1) {
      this.data = [];
    }
    this.data = this.data.slice(0, idx).concat(this.data.slice(idx + 1));
    return this;
  }

  indexOf(id) {
    const ids = this.data.map(e => e.id);

    return ids.indexOf(id);
  }

  move(idx, newIdx) {
    if (idx === -1 || newIdx === -1) {
      return this;
    }

    let offset = 0;
    const currentEl = this.get(idx);

    this.delete(idx);

    if (newIdx <= 0) {
      return this.unshift(currentEl);
    } else if (newIdx >= this.data.length) {
      return this.push(currentEl);
    }

    if (newIdx > idx) {
      offset = 1;
    }

    this.data = this.data
      .slice(0, newIdx - offset)
      .concat([currentEl])
      .concat(this.data.slice(newIdx - offset));

    return this;
  }
}

export function getDefaultStructureElement(type) {
  if (type === 'columns') {
    return Columns.default;
  } else if (type === 'articles') {
    return Articles.default;
  } else if (type === 'elements') {
    return Elements.default;
  } else if (type === 'fullwidth') {
    return FullWidth.default;
  }
  return null;
}
