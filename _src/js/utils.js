export function rejectEmpty(x) {
  return Boolean(x);
}

export function reloadPage() {
  return fetch('', {credentials: 'include'})
    .then(response => response.text())
    .then(text => {
      document.documentElement.innerHTML = '';
      document.open();
      document.write(text);
      document.close();
    });
}

export function getElementID(el) {
  if (!el) {
    return -1;
  }
  const container = el.closest('.container');
  if (!container) {
    return -1;
  }

  return parseInt(container.getAttribute('data-id'), 10);
}

