import FullWidth from './types/fullwidth';
import Columns from './types/columns';
import Articles from './types/articles';
import Elements from './types/elements';
import AddNew from './types/add_new';

import {reloadPage} from './utils';
import {getDefaultStructureElement} from './structure';

const availableTypes = [
  {
    title: Columns.texts.title,
    value: 'columns'
  },
  {
    title: FullWidth.texts.title,
    value: 'fullwidth'
  },
  {
    title: Articles.texts.title,
    value: 'articles'
  },
  {
    title: Elements.texts.title,
    value: 'elements'
  }
];

function initAddNewEditor(btn, pageData, structure) {
  btn.removeAttribute('disabled');
  return new Edicy.SettingsEditor(btn, {
    menuItems: [
      {
        title: AddNew.texts.title,
        type: 'select',
        key: 'type',
        list: availableTypes
      }
    ],

    values: {},

    commit(data) {
      structure.push(getDefaultStructureElement(data.type));
      pageData.set(
        'structure',
        {
          structure: structure.value
        },
        {
          success: reloadPage
        }
      );
    }
  });
}

function initBgPicker(btn, pageData, structure) {
  const container = btn.closest('.container');
  const id = parseInt(container.getAttribute('data-id'), 10);
  const bgPicker = new Edicy.BgPicker(btn, {
    showAlpha: true,
    picture: false,
    preview(data) {
      container.setAttribute('style', `background-color: ${data.color}`);
    },
    commit(data) {
      const idx = structure.indexOf(id);
      const el = Object.assign({}, structure.get(idx));
      el.data = el.data || {};
      el.data.color = data.color;
      el.data.colorData = data.colorData;
      structure.replace(idx, el);
      pageData.set('structure', {
        structure: structure.value
      });
    }
  });
}

function initEditor(btn, pageData, structure) {
  const type = btn.getAttribute('data-type');

  if (type === 'columns') {
    Columns.initEditor(btn, pageData, structure);
  } else if (type === 'articles') {
    Articles.initEditor(btn, pageData, structure);
  } else if (type === 'elements') {
    Elements.initEditor(btn, pageData, structure);
  } else if (type === 'add_new') {
    initAddNewEditor(btn, pageData, structure);
  }
}

export function initEditors(structure, pageData) {
  const settingsBtns = document.querySelectorAll('.js-settings-btn');
  const bgBtns = document.querySelectorAll('.container .bg-settings-toggle');
  Array.from(settingsBtns).map((btn, index) => initEditor(btn, pageData, structure));
  Array.from(bgBtns).map((btn, index) => initBgPicker(btn, pageData, structure));
}
