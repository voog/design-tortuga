import {initEditors} from './editors';
import {initDrag} from './drag';
import {Structure} from './structure';
import {reloadPage, getElementID} from './utils';

window.init = function init(opts) {
  const {pageData} = opts;
  const structure = new Structure(opts.structure ? opts.structure.structure : []);

  initEditors(structure, pageData);

  const drake = initDrag();
  drake.on('drop', (el, target, source, sibling) => {
    const thisIdx = structure.indexOf(getElementID(el));
    const otherIdx = sibling ? structure.indexOf(getElementID(sibling)) : structure.value.length;

    pageData.set('structure', {structure: structure.move(thisIdx, otherIdx).value});
  });

  document.querySelector('.container-list').addEventListener('click', event => {
    const btn = event.target.closest('.js-delete-btn');
    if (btn) {
      const id = getElementID(btn);
      structure.delete(structure.indexOf(id));

      pageData.set('structure', {structure: structure.value}, {success: reloadPage});
    }
  });
};
