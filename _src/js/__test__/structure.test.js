import {Structure} from '../structure';

const data = [{id: 1}, {id: 2}, {id: 3}, {id: 4}, {id: 5}];

describe('Structure', () => {
  describe('#update', () => {
    test('updating an element', () => {
      const structure = new Structure(data);
      expect(structure.update(0, {value: 'foobar'}).value).toEqual([
        {id: 1, value: 'foobar'},
        {id: 2},
        {id: 3},
        {id: 4},
        {id: 5}
      ]);
    });
  });

  describe('#replace', () => {
    test('replacing an element', () => {
      const structure = new Structure(data);
      expect(structure.replace(0, {value: 'foobar'}).value).toEqual([
        {value: 'foobar'},
        {id: 2},
        {id: 3},
        {id: 4},
        {id: 5}
      ]);
    });
  });

  describe('#push', () => {
    test('adding an element to the end (with id)', () => {
      const structure = new Structure(data);
      expect(structure.push({id: 6}).value).toEqual([{id: 1}, {id: 2}, {id: 3}, {id: 4}, {id: 5}, {id: 6}]);
    });
    test('adding an element to the end (without id)', () => {
      const structure = new Structure(data);
      expect(structure.push({}).value).toEqual([{id: 1}, {id: 2}, {id: 3}, {id: 4}, {id: 5}, {id: 6}]);
    });
  });

  describe('#unshift', () => {
    test('adding an element to the start (with id)', () => {
      const structure = new Structure(data);
      expect(structure.unshift({id: 6}).value).toEqual([{id: 6}, {id: 1}, {id: 2}, {id: 3}, {id: 4}, {id: 5}]);
    });
    test('adding an element to the start (without id)', () => {
      const structure = new Structure(data);
      expect(structure.unshift({}).value).toEqual([{id: 6}, {id: 1}, {id: 2}, {id: 3}, {id: 4}, {id: 5}]);
    });
  });

  describe('#getNextID', () => {
    test('with no elements', () => {
      const structure = new Structure([]);
      expect(structure.getNextID()).toEqual(1);
    });
    test('with all of the elements having ids', () => {
      const structure = new Structure(data);
      expect(structure.getNextID()).toEqual(6);
    });
    test('with some of the elements having ids', () => {
      const structure = new Structure(data);
      structure.push({}).push({});
      expect(structure.getNextID()).toEqual(8);
    });
    test('with none of the elements having ids', () => {
      const structure = new Structure([{}, {}, {}, {}]);
      expect(structure.getNextID()).toEqual(4);
    });
  });

  describe('#delete', () => {
    test('deleting an element', () => {
      const structure = new Structure(data);
      expect(structure.delete(-1).value).toEqual(data);
      expect(structure.delete(100).value).toEqual(data);
      expect(structure.delete(0).value).toEqual([{id: 2}, {id: 3}, {id: 4}, {id: 5}]);
      expect(structure.delete(3).value).toEqual([{id: 2}, {id: 3}, {id: 4}]);
    });
  });

  describe('#move', () => {
    test('moving first element to middle', () => {
      const structure = new Structure(data);
      expect(structure.move(0, 3).value).toEqual([{id: 2}, {id: 3}, {id: 1}, {id: 4}, {id: 5}]);
    });

    test('moving first element to end', () => {
      const structure = new Structure(data);
      expect(structure.move(0, 5).value).toEqual([{id: 2}, {id: 3}, {id: 4}, {id: 5}, {id: 1}]);
    });

    test('moving middle element to end', () => {
      const structure = new Structure(data);
      expect(structure.move(2, 5).value).toEqual([{id: 1}, {id: 2}, {id: 4}, {id: 5}, {id: 3}]);
    });

    test('moving middle element to start', () => {
      const structure = new Structure(data);
      expect(structure.move(2, 0).value).toEqual([{id: 3}, {id: 1}, {id: 2}, {id: 4}, {id: 5}]);
    });

    test('moving last element to middle', () => {
      const structure = new Structure(data);
      expect(structure.move(4, 2).value).toEqual([{id: 1}, {id: 2}, {id: 5}, {id: 3}, {id: 4}]);
    });

    test('moving last element to start', () => {
      const structure = new Structure(data);
      expect(structure.move(4, 0).value).toEqual([{id: 5}, {id: 1}, {id: 2}, {id: 3}, {id: 4}]);
    });

    test('not moving at all', () => {
      const structure = new Structure(data);
      expect(structure.move(0, 0).value).toEqual([{id: 1}, {id: 2}, {id: 3}, {id: 4}, {id: 5}]);
      expect(structure.move(2, 2).value).toEqual([{id: 1}, {id: 2}, {id: 3}, {id: 4}, {id: 5}]);
      expect(structure.move(4, 4).value).toEqual([{id: 1}, {id: 2}, {id: 3}, {id: 4}, {id: 5}]);
    });
  });
});
