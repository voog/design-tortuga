<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
    <link rel="stylesheet" href="{{ stylesheets_path }}/main.css">
    
    <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>

    {% if editmode %}
      <link rel="stylesheet"
        href="{{ site.static_asset_host }}/libs/edicy-tools/latest/edicy-tools.css"
      >
    {% endif %}
    
    <title>Tortuga DEMO</title>
  </head>
  <body data-structure="{{ page.data.structure | json | escape }}">
    <div class="header">
      {% content name="header" %}
    </div>
    <div class="container-list">
      {% include 'element_box' element: element, editable: true %}
    </div>
  </body>
</html>
