<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dragula/3.7.2/dragula.min.css" />
    <link rel="stylesheet" href="{{ stylesheets_path }}/main.css">
    
    <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dragula/3.7.2/dragula.min.js"></script>
    <script src="{{ javascripts_path }}/main.js"></script>

    {% if editmode %}
      <link rel="stylesheet"
        href="{{ site.static_asset_host }}/libs/edicy-tools/latest/edicy-tools.css"
      >
    {% endif %}
    
    <title>Tortuga DEMO</title>
  </head>
  <body data-structure="{{ page.data.structure | json | escape }}">
    <div class="header">
      {% content name="header" %}
    </div>
    <div class="container-list">
      {% if page.data.structure %}
        {% assign structure = page.data.structure  %}
      {% else %}
        {% assign structure = '{"structure": []}' | parse_json | escape %}
      {% endif %}
      
      {% for el in structure.structure %}
        {% assign type = el.type %}
        {% include type block: el %}
      {% endfor %}
    </div>
    <div>
      {% if editmode %}
        <button disabled class="js-settings-btn" data-type="add_new">Add new</button>
      {% endif %}
    </div>
    
    {% editorjsblock %}
      <script
        src="{{ site.static_asset_host }}/libs/edicy-tools/latest/edicy-tools.js"
      ></script>
    
      <script>
        var pageData = new Edicy.CustomData({
          type: 'page',
          id: {{ page.id }}
        });

        var Tortuga = init({
          structure: JSON.parse('{{ structure | json }}'),
          pageData: pageData,
          Edicy: Edicy
        });
      </script>
    {% endeditorjsblock %}
  </body>
</html>
