(function (dragula) {
'use strict';

dragula = dragula && dragula.hasOwnProperty('default') ? dragula['default'] : dragula;

function construct() {
  return {
    type: 'fullwidth'
  };
}

var FullWidth = {
  texts: {
    title: 'Full width content'
  },
  options: {},
  default: construct(),
  construct: construct
};

function reloadPage() {
  return fetch('', { credentials: 'include' }).then(function (response) {
    return response.text();
  }).then(function (text) {
    document.documentElement.innerHTML = '';
    document.open();
    document.write(text);
    document.close();
  });
}

function getElementID(el) {
  if (!el) {
    return -1;
  }
  var container = el.closest('.container');
  if (!container) {
    return -1;
  }

  return parseInt(container.getAttribute('data-id'), 10);
}

var options = {
  min: 1,
  max: 3
};

var texts = {
  title: 'Vertical columns'
};

function construct$1(opts) {
  return {
    type: 'columns',
    data: {
      count: opts.count
    }
  };
}

function initEditor$1(btn, pageData, structure) {
  var data = btn.getAttribute('data-value');
  var id = getElementID(btn);
  var start = options.min;
  var end = options.max;
  var length = end - start + 1;

  btn.removeAttribute('disabled');
  return new Edicy.SettingsEditor(btn, {
    menuItems: [{
      title: texts.title,
      type: 'select',
      key: 'count',
      list: Array.from(Array(length).keys()).map(function (idx) {
        var n = idx + start;
        return {
          title: String(n),
          value: n
        };
      })
    }],

    values: {
      count: parseInt(data, 10)
    },

    commit: function commit(data) {
      var idx = structure.indexOf(id);
      var el = Object.assign({}, structure.get(idx));
      el.data = el.data || {};
      el.data.count = data.count;
      structure.update(idx, el);
      console.log(structure.value);
      pageData.set('structure', {
        structure: structure.value
      }, {
        success: reloadPage
      });
    }
  });
}

var Columns = {
  texts: texts,
  options: options,
  default: construct$1({ count: 1 }),
  construct: construct$1,
  initEditor: initEditor$1
};

var options$1 = {
  min: 1,
  max: 5
};

var texts$1 = {
  title: 'Blog articles'
};

function construct$2(opts) {
  return {
    type: 'articles',
    data: {
      count: opts.count,
      path: 'blog'
    }
  };
}

function initEditor$2(btn, pageData, structure) {
  var data = btn.getAttribute('data-value');
  var id = getElementID(btn);
  var start = options$1.min;
  var end = options$1.max;
  var length = end - start + 1;

  btn.removeAttribute('disabled');
  return new Edicy.SettingsEditor(btn, {
    menuItems: [{
      title: texts$1.title,
      type: 'select',
      key: 'count',
      list: Array.from(Array(length).keys()).map(function (idx) {
        var n = idx + start;
        return {
          title: String(n),
          value: n
        };
      })
    }],

    values: {
      count: parseInt(data, 10)
    },

    commit: function commit(data) {
      var idx = structure.indexOf(id);
      var updatedEl = Object.assign({}, structure.get(idx), { data: { count: data.count } });
      structure.update(idx, updatedEl);
      pageData.set('structure', {
        structure: structure.value
      }, {
        success: reloadPage
      });
    }
  });
}

var Articles = {
  texts: texts$1,
  options: options$1,
  default: construct$2({ count: 3 }),
  construct: construct$2,
  initEditor: initEditor$2
};

var options$2 = {
  min: 1,
  max: 5
};

var texts$2 = {
  title: 'Element list'
};

function construct$3(opts) {
  return {
    type: 'elements',
    data: {
      count: opts.count,
      path: 'elements'
    }
  };
}

function initEditor$3(btn, pageData, structure) {
  var data = btn.getAttribute('data-value');
  var id = getElementID(btn);
  var start = options$2.min;
  var end = options$2.max;
  var length = end - start + 1;

  btn.removeAttribute('disabled');
  return new Edicy.SettingsEditor(btn, {
    menuItems: [{
      title: texts$2.title,
      type: 'select',
      key: 'count',
      list: Array.from(Array(length).keys()).map(function (idx) {
        var n = idx + start;
        return {
          title: String(n),
          value: n
        };
      })
    }],

    values: {
      count: parseInt(data, 10)
    },

    commit: function commit(data) {
      var idx = structure.indexOf(id);
      var el = Object.assign({}, structure.get(idx));
      el.data = el.data || {};
      el.data.count = data.count;
      structure.update(idx, el);
      pageData.set('structure', {
        structure: structure.value
      }, {
        success: reloadPage
      });
    }
  });
}

var Elements = {
  texts: texts$2,
  options: options$2,
  default: construct$3({ count: 3 }),
  construct: construct$3,
  initEditor: initEditor$3
};

var texts$3 = {
  title: 'Add new structure element'
};

var AddNew = {
  texts: texts$3
};

var asyncGenerator = function () {
  function AwaitValue(value) {
    this.value = value;
  }

  function AsyncGenerator(gen) {
    var front, back;

    function send(key, arg) {
      return new Promise(function (resolve, reject) {
        var request = {
          key: key,
          arg: arg,
          resolve: resolve,
          reject: reject,
          next: null
        };

        if (back) {
          back = back.next = request;
        } else {
          front = back = request;
          resume(key, arg);
        }
      });
    }

    function resume(key, arg) {
      try {
        var result = gen[key](arg);
        var value = result.value;

        if (value instanceof AwaitValue) {
          Promise.resolve(value.value).then(function (arg) {
            resume("next", arg);
          }, function (arg) {
            resume("throw", arg);
          });
        } else {
          settle(result.done ? "return" : "normal", result.value);
        }
      } catch (err) {
        settle("throw", err);
      }
    }

    function settle(type, value) {
      switch (type) {
        case "return":
          front.resolve({
            value: value,
            done: true
          });
          break;

        case "throw":
          front.reject(value);
          break;

        default:
          front.resolve({
            value: value,
            done: false
          });
          break;
      }

      front = front.next;

      if (front) {
        resume(front.key, front.arg);
      } else {
        back = null;
      }
    }

    this._invoke = send;

    if (typeof gen.return !== "function") {
      this.return = undefined;
    }
  }

  if (typeof Symbol === "function" && Symbol.asyncIterator) {
    AsyncGenerator.prototype[Symbol.asyncIterator] = function () {
      return this;
    };
  }

  AsyncGenerator.prototype.next = function (arg) {
    return this._invoke("next", arg);
  };

  AsyncGenerator.prototype.throw = function (arg) {
    return this._invoke("throw", arg);
  };

  AsyncGenerator.prototype.return = function (arg) {
    return this._invoke("return", arg);
  };

  return {
    wrap: function (fn) {
      return function () {
        return new AsyncGenerator(fn.apply(this, arguments));
      };
    },
    await: function (value) {
      return new AwaitValue(value);
    }
  };
}();





var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();









































var toConsumableArray = function (arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

    return arr2;
  } else {
    return Array.from(arr);
  }
};

var Structure = function () {
  function Structure(data) {
    classCallCheck(this, Structure);

    if (!data) {
      data = [];
    }
    this.data = [].concat(toConsumableArray(data));
  }

  createClass(Structure, [{
    key: 'get',
    value: function get$$1(idx) {
      return this.data[idx];
    }
  }, {
    key: 'update',
    value: function update(idx, value) {
      if (idx < 0 || idx >= this.data.length) {
        return this;
      }
      this.data[idx] = Object.assign({}, this.data[idx], value);
      return this;
    }
  }, {
    key: 'replace',
    value: function replace(idx, value) {
      this.data[idx] = value;
      return this;
    }
  }, {
    key: 'getNextID',
    value: function getNextID() {
      if (this.data.length === 0) {
        return 1;
      }
      var ids = this.data.map(function (e, idx) {
        return typeof e.id !== 'undefined' ? e.id : idx;
      });
      return Math.max.apply(Math, toConsumableArray(ids)) + 1;
    }
  }, {
    key: 'push',
    value: function push(value) {
      var idMissing = typeof value.id === 'undefined';
      this.data.push(Object.assign(value, idMissing ? { id: this.getNextID() } : {}));
      return this;
    }
  }, {
    key: 'unshift',
    value: function unshift(value) {
      var idMissing = typeof value.id === 'undefined';
      this.data.unshift(Object.assign(value, idMissing ? { id: this.getNextID() } : {}));
      return this;
    }
  }, {
    key: 'delete',
    value: function _delete(idx) {
      if (idx === -1) {
        return this;
      } else if (idx <= 1 && this.value.length === 1) {
        this.data = [];
      }
      this.data = this.data.slice(0, idx).concat(this.data.slice(idx + 1));
      return this;
    }
  }, {
    key: 'indexOf',
    value: function indexOf(id) {
      var ids = this.data.map(function (e) {
        return e.id;
      });

      return ids.indexOf(id);
    }
  }, {
    key: 'move',
    value: function move(idx, newIdx) {
      if (idx === -1 || newIdx === -1) {
        return this;
      }

      var offset = 0;
      var currentEl = this.get(idx);

      this.delete(idx);

      if (newIdx <= 0) {
        return this.unshift(currentEl);
      } else if (newIdx >= this.data.length) {
        return this.push(currentEl);
      }

      if (newIdx > idx) {
        offset = 1;
      }

      this.data = this.data.slice(0, newIdx - offset).concat([currentEl]).concat(this.data.slice(newIdx - offset));

      return this;
    }
  }, {
    key: 'value',
    get: function get$$1() {
      return this.data;
    }
  }]);
  return Structure;
}();

function getDefaultStructureElement(type) {
  if (type === 'columns') {
    return Columns.default;
  } else if (type === 'articles') {
    return Articles.default;
  } else if (type === 'elements') {
    return Elements.default;
  } else if (type === 'fullwidth') {
    return FullWidth.default;
  }
  return null;
}

var availableTypes = [{
  title: Columns.texts.title,
  value: 'columns'
}, {
  title: FullWidth.texts.title,
  value: 'fullwidth'
}, {
  title: Articles.texts.title,
  value: 'articles'
}, {
  title: Elements.texts.title,
  value: 'elements'
}];

function initAddNewEditor(btn, pageData, structure) {
  btn.removeAttribute('disabled');
  return new Edicy.SettingsEditor(btn, {
    menuItems: [{
      title: AddNew.texts.title,
      type: 'select',
      key: 'type',
      list: availableTypes
    }],

    values: {},

    commit: function commit(data) {
      structure.push(getDefaultStructureElement(data.type));
      pageData.set('structure', {
        structure: structure.value
      }, {
        success: reloadPage
      });
    }
  });
}

function initBgPicker(btn, pageData, structure) {
  var container = btn.closest('.container');
  var id = parseInt(container.getAttribute('data-id'), 10);
  var bgPicker = new Edicy.BgPicker(btn, {
    showAlpha: true,
    picture: false,
    preview: function preview(data) {
      container.setAttribute('style', 'background-color: ' + data.color);
    },
    commit: function commit(data) {
      var idx = structure.indexOf(id);
      var el = Object.assign({}, structure.get(idx));
      el.data = el.data || {};
      el.data.color = data.color;
      el.data.colorData = data.colorData;
      structure.replace(idx, el);
      pageData.set('structure', {
        structure: structure.value
      });
    }
  });
}

function initEditor(btn, pageData, structure) {
  var type = btn.getAttribute('data-type');

  if (type === 'columns') {
    Columns.initEditor(btn, pageData, structure);
  } else if (type === 'articles') {
    Articles.initEditor(btn, pageData, structure);
  } else if (type === 'elements') {
    Elements.initEditor(btn, pageData, structure);
  } else if (type === 'add_new') {
    initAddNewEditor(btn, pageData, structure);
  }
}

function initEditors(structure, pageData) {
  var settingsBtns = document.querySelectorAll('.js-settings-btn');
  var bgBtns = document.querySelectorAll('.container .bg-settings-toggle');
  Array.from(settingsBtns).map(function (btn, index) {
    return initEditor(btn, pageData, structure);
  });
  Array.from(bgBtns).map(function (btn, index) {
    return initBgPicker(btn, pageData, structure);
  });
}

function initDrag() {
  return dragula(Array.from(document.querySelectorAll('.container-list')), {
    moves: function moves(el, source, handle, sibling) {
      return el.classList.contains('container');
    },


    revertOnSpill: true,
    removeOnSpill: false,
    ignoreInputTextSelection: true
  });
}

window.init = function init(opts) {
  var pageData = opts.pageData;

  var structure = new Structure(opts.structure ? opts.structure.structure : []);

  initEditors(structure, pageData);

  var drake = initDrag();
  drake.on('drop', function (el, target, source, sibling) {
    var thisIdx = structure.indexOf(getElementID(el));
    var otherIdx = sibling ? structure.indexOf(getElementID(sibling)) : structure.value.length;

    pageData.set('structure', { structure: structure.move(thisIdx, otherIdx).value });
  });

  document.querySelector('.container-list').addEventListener('click', function (event) {
    var btn = event.target.closest('.js-delete-btn');
    if (btn) {
      var id = getElementID(btn);
      structure.delete(structure.indexOf(id));

      pageData.set('structure', { structure: structure.value }, { success: reloadPage });
    }
  });
};

}(dragula));
