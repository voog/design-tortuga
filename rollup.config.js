import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import babel from 'rollup-plugin-babel';

export default [
  {
    input: '_src/js/main.js',
    output: {
      format: 'iife',
      file: 'javascripts/main.js'
    },
    plugins: [
      babel({
        exclude: 'node_modules/**'
      }),
      resolve(),
      commonjs()
    ],
    external: ['dragula']
  }
];
